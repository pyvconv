# Pyvconv - A simple frontend for ffmpeg/mencoder
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import sys
import os
import gtk
import gobject
import gtk.glade

from command_executer import CommandExecuterView
from command_executer import CommandExecuter

class GtkCommandExecuterView(CommandExecuterView):

    def on_abortbutton_clicked(self, data):
        if self.executor:
            self.executor.abort()
            self.progresswindow.destroy()

    def __init__(self):
        CommandExecuterView.__init__(self)
        self.widgets = gtk.glade.XML("progresswin.glade")
        self.progresswindow = self.widgets.get_widget("progresswindow")
        self.progresslabel = self.widgets.get_widget("progresslabel")
        self.abortbutton = self.widgets.get_widget("abortbutton")
        self.abortbutton.connect("clicked", self.on_abortbutton_clicked)
        self.progresswindow.show_all()

        self.executor = None

    def recieve_executor(self, executor):
        self.executor = executor

    def starting_conversion(self, infile, index, total, outfile):
        gtk.gdk.threads_enter()
        self.progresswindow.set_title("Converting " + os.path.basename(infile) + " (" + str(index) + "/" + str(total) + ")")
        gtk.gdk.threads_leave()

    def update_progress(self, logline):
        gtk.gdk.threads_enter()
        self.progresslabel.set_text(logline)
        gtk.gdk.threads_leave()
    def error(self):
        gtk.MessageDialog(message_format="There was an error during the conversion", type=gtk.MESSAGE_ERROR).show()
        self.progresswindow.destroy()

    def finished_all(self):
        gtk.MessageDialog(message_format="All conversions succeded :)").show()
        self.progresswindow.destroy()

class MyGui:
    last_add_folder_str = ""
    last_convert_folder_str = ""

    def _enable_or_disable_convertbutton(self):
        if len(self.file_list) > 0 and len(self.outdirentry.get_text()) > 0:
            self.convertbutton.set_sensitive(True)
        else:
            self.convertbutton.set_sensitive(False)

    def on_treeview_click_row(self, data):
        self.removebutton.set_sensitive(True)

    def on_removebutton_clicked(self, data):
        selection = self.tree_view.get_selection()
        (model, index) = selection.get_selected_rows()
        for i in index:
            itmtoremove = self.tree_store.get(self.tree_store.iter_nth_child(None, i[0]), 0)[0]
            self.file_list.remove(itmtoremove)
        self.tree_store.clear()
        for f in self.file_list:
            self.tree_store.append(None, [f])
        self.tree_view.expand_all()
        self.removebutton.set_sensitive(False)

        self._enable_or_disable_convertbutton()

    def on_addbutton_clicked(self, data):
        chooser = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                  buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        chooser.set_select_multiple(True)
	if self.last_add_folder_str:
	    chooser.set_current_folder(self.last_add_folder_str)

        ffilter = gtk.FileFilter()
        ffilter.set_name("Videos")
        ffilter.add_mime_type("video/*")
        chooser.add_filter(ffilter)

        response = chooser.run()
        if response == gtk.RESPONSE_OK:
	    self.last_add_folder_str = chooser.get_current_folder()

            for f in chooser.get_filenames():
                if f not in self.file_list:
                    self.file_list.append(f)
            self.tree_store.clear()
            for f in self.file_list:
                self.tree_store.append(None, [f])
            self.tree_view.expand_all()
            self.removebutton.set_sensitive(False)

            self._enable_or_disable_convertbutton()

        chooser.destroy()

    def on_outdirbrowsebutton_clicked(self, data):
        chooser = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                  buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))

        if self.last_convert_folder_str:
            chooser.set_current_folder(self.last_convert_folder_str)
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            self.last_convert_folder_str = chooser.get_current_folder()
            self.outdirentry.set_text(chooser.get_filename())
            self._enable_or_disable_convertbutton()
        chooser.destroy()

    def on_convertbutton_clicked(self, data):
        command = self.commands[self.combobox.get_active_text()]
        outdir = self.outdirentry.get_text()

        CommandExecuter(GtkCommandExecuterView(), command, self.file_list, outdir)

    def __init__(self, commands):

        if 0 == len(commands):
            dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_ERROR, gtk.BUTTONS_OK)
            dialog.set_markup("None of the programs needed for the conversions specified in commands.xml were found. Cannot continue")
            dialog.run()
            dialog.destroy()
            sys.exit(-1)

        self.file_list = []
        self.commands = commands

        self.widgets = gtk.glade.XML("mainwin.glade")

        self.tree_store = gtk.TreeStore(gobject.TYPE_STRING)

        self.tree_view     = self.widgets.get_widget("fileview")
        self.combobox      = self.widgets.get_widget("commandcombo")
        self.outdirentry   = self.widgets.get_widget("outdirentry")
        self.removebutton  = self.widgets.get_widget("removebutton")
        self.convertbutton = self.widgets.get_widget("convertbutton")
        self.window        = self.widgets.get_widget("window1")

        self.tree_view.set_model(self.tree_store)
        r = gtk.CellRendererText()
        p_column = gtk.TreeViewColumn("", r)
        p_column.add_attribute(r,"text",0)
        selection = self.tree_view.get_selection()
        selection.set_mode(gtk.SELECTION_MULTIPLE)
        selection.connect("changed", self.on_treeview_click_row)

        self.tree_view.insert_column(p_column,0)
        self.tree_view.set_rules_hint(True)

        self.widgets.get_widget("addbutton").connect("clicked", self.on_addbutton_clicked)
        self.removebutton.connect("clicked", self.on_removebutton_clicked)
        self.widgets.get_widget("outdirbrowsebutton").connect("clicked", self.on_outdirbrowsebutton_clicked)

        self.convertbutton.connect("clicked", self.on_convertbutton_clicked)

        self.combobox.set_model(gtk.ListStore(str))
        cell = gtk.CellRendererText()
        self.combobox.pack_start(cell, True)
        self.combobox.add_attribute(cell, 'text', 0)

        for c in commands:
            self.combobox.append_text(c.get_name())
        self.combobox.set_active(0)

        self.window.connect("delete-event", gtk.main_quit)
        self.window.show_all()

        gtk.gdk.threads_enter()
        gtk.main()
        gtk.gdk.threads_leave()

