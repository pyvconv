# Pyvconv - A simple frontend for ffmpeg/mencoder
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import re
import os
import time
import datetime
import subprocess
import thread
import threading
import signal
from commands import Variable

class CommandExecuterView:
    def __init__(self):
        pass

    def recieve_executor(self, executor):
        pass

    def starting_conversion(self, infile, index, total, outfile):
        pass

    def update_progress(self, logline):
        pass

    def error(self):
        pass

    def finished_all(self):
        pass

class CommandExecuter:
    def mk_unique_subdir(self, parentdir):
        p = re.compile("[\ ,\/,\(,\)]")
        namepart = p.sub("_", self.command.get_name())

        t = datetime.datetime.now()
        newdir = os.path.join(parentdir, namepart + "_" + str(time.mktime(t.timetuple())))

        if os.path.isdir(newdir):
            raise OSError("Unable to make up unique name for new folder")
        elif os.path.isfile(newdir):
            raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)
        else:
            os.mkdir(newdir)
            return newdir

    def __init__(self, cmdexecview, command, infilelist, outdir):
        self.cmdexecview = cmdexecview
        self.command = command
        self.infilelist = infilelist
        self.outdir = self.mk_unique_subdir(outdir)

        self.please_abort = False

        self.shared_vars_lock = threading.Lock()
        thread.start_new_thread(self._run_conversion, ())

    def abort(self):
        self.shared_vars_lock.acquire()
        self.please_abort = True
        self.shared_vars_lock.release()

    def _escape_filepath(self, path):
        return "\"" + path  +  "\""

    def _run_conversion(self):
        index = 0
        total = len(self.infilelist)

        for infile in self.infilelist:
            newbase = os.path.basename(infile)
            newbase = newbase[0: min(newbase.rfind("."), len(newbase))] + "." + self.command.get_outfile_extension()

            outfile = os.path.join(self.outdir, newbase)

            index = index + 1

            self.command.put_var(Variable("in",  self._escape_filepath(infile)))
            self.command.put_var(Variable("out",  self._escape_filepath(outfile)))

            self.cmdexecview.starting_conversion(infile, index, total, outfile)
            p = subprocess.Popen(str(self.command), shell = True, universal_newlines=True, stderr = subprocess.PIPE)
            ostream = p.stderr

            self.cmdexecview.recieve_executor(self)

            newline = str(chr(32) * 4)
            buff = ""
            c = 1
            iskip = 10
            skipc = 0

            while c:
                c = ostream.read(1)

                buff = buff + c
                if buff.endswith(newline):
                    skipc = (skipc + 1) % iskip
                    if skipc == 0:
                        self.cmdexecview.update_progress(buff.strip())
                    buff = ""

                self.shared_vars_lock.acquire()
                if self.please_abort == True:
                    ostream.close()
                    os.kill(p.pid, signal.SIGTERM)
                    return
                self.shared_vars_lock.release()

            ostream.close()

            res = p.wait()

            if res < 0:
                self.cmdexecview.error()
                return

        self.cmdexecview.finished_all()
