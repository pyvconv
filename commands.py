# Pyvconv - A simple frontend for ffmpeg/mencoder
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import os
import libxml2
import re

class Error(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)

class Variable:
    def __init__(self, namestr, defaultstr, exprstr = None):
        self.namestr = namestr
        self.defaultstr = defaultstr
        self.exprstr = exprstr

    def __str__(self):
        return self.namestr

    def has_value(self):
        return self.defaultstr != None

    def get_value(self):
        if self.defaultstr == None:
            raise Error("Error: Trying to eval variable " + self.namestr + " with no value")
        return self.defaultstr

class OptionalSetting:
    def __init__(self, namestr, exprstr):
        self.namestr = namestr
        self.exprstr = exprstr

    def get_name(self):
        return self.namestr

    def get_value(self, var_list):
        instr = self.exprstr
        for var in var_list:
            if var_list[var].has_value():
                p = re.compile( "\$\{" + var + "\}")
                instr = p.sub( var_list[var].get_value(), instr)
        if instr.find("$") != -1:
            instr = "" # if not all variables were found, return empty option
        return instr

class Command:
    def __init__(self, namestr, callstr, extstr, var_list, optset_list):
        self.namestr = namestr
        self.callstr = callstr
        self.extstr = extstr
        self.var_list = var_list
        self.optset_list = optset_list

    def get_name(self):
        return self.namestr

    def get_outfile_extension(self):
        return self.extstr

    def put_var(self, var):
        self.var_list[str(var)] = var

    def get_vars(self):
        return self.var_list

    def __str__(self):
        instr = self.callstr

        p = re.compile("\$\[(\w+)\]")
        for o in p.findall(instr):
            if o not in self.optset_list:
                raise Error("Error: Optional variable not defined")
            instr = p.sub(self.optset_list[o].get_value(self.var_list), instr)

        p = re.compile("\$\{(\w+)\}")
        for o in p.findall(instr):
            if o not in self.var_list:
                raise Error("Error: Variable \"" + o + "\" has not been assigned")
            val = self.var_list[o].get_value()
            instr = instr.replace("${" + o + "}", val)

        if instr.find("$") != -1:
            raise Error("Error: all variables were not expanded")
        return instr

class CommandList:
    def __init__(self):
        self.cmdcall_dict = {}
        self._read_config()

    def _read_properties(self, propnode, mand_propnames, opt_propnames = []):
        propdict = {}

        for p in propnode:
            if p.name in mand_propnames:
                propdict[p.name] = str(p.content)
            elif p.name != "text" and p.name not in opt_propnames:
                raise Error("Error parsing XML: only name and call are accepted properties in the element command, found " + str(p.name))

        if len(mand_propnames) != len(propdict.keys()):
            raise Error("Error parsing XML: must supply both name and call in element command")

        for p in propnode:
            if p.name in opt_propnames:
                propdict[p.name] = str(p.content)
            elif p.name != "text" and p.name not in mand_propnames:
                raise Error("Error parsing XML: only name and call are accepted properties in the element command, found " + str(p.name))

        return propdict

    def _get_config_path(self):
        homeconf = os.path.expanduser("~") + "/.pyvconv/commands.xml"
        if os.path.isfile("commands.xml"):
            return "commands.xml"
        elif os.path.isfile(homeconf):
            return homeconf
        else:
            raise Error("Error: No config found")

    def _cmd_program_found_in_path(self, programstr):
        if False == self.program_boolexists_dict.has_key(programstr):

            if not os.environ.has_key('PATH') or os.environ['PATH']=='':
                p = os.defpath
            else:
                p = os.environ['PATH']

            self.program_boolexists_dict[programstr] = False # not found yet, so set to false

            pathlist = p.split (os.pathsep)
            for path in pathlist:
                f = os.path.join(path, programstr)
                if os.access(f, os.X_OK):
                    self.program_boolexists_dict[programstr] = True

        return self.program_boolexists_dict[programstr]

    def _read_config(self):
        doc = libxml2.parseFile(self._get_config_path())
        cmd_xmllist = doc.xpathEval( '//command')

        self.program_boolexists_dict = {}

    	# read all commands from XML description
        for cmdnode in cmd_xmllist:
            if cmdnode.type == "element" and cmdnode.name == "command":

                var_list = {}
                optset_list = {}

                if cmdnode.children:
                    for varsetnode in cmdnode.children:
                        if varsetnode.type == "element" and varsetnode.name == "variable":
                            props =  self._read_properties(varsetnode.properties, ["name", "expr"], ["default"])
                            defprop = None
                            if "default" in props:
                                defprop = props["default"]
                            var_list[props["name"]] = Variable(props["name"], defprop, props["expr"])

                        elif varsetnode.type == "element" and varsetnode.name == "optionalsetting":
                            props = self._read_properties(varsetnode.properties, ["name", "expr"])
                            optset_list[props["name"]] = OptionalSetting(props["name"], props["expr"])

                        elif varsetnode.name != "text":
                            raise Error("Error parsing XML: only variable and optionalsetting elements allowed in command")

                props = self._read_properties(cmdnode.properties, ["name", "call", "ext"])

                if self._cmd_program_found_in_path( props["call"].split()[0] ):
                    self.cmdcall_dict[props["name"]] = Command(props["name"], props["call"], props["ext"], var_list, optset_list)

            else:
                raise Error("Error parsing XML: only command elements are supported")

    def __iter__(self):
        return self.cmdcall_dict.values().__iter__()

    def __len__(self):
        return len(self.cmdcall_dict)

    def __getitem__(self, name):
        return self.cmdcall_dict[name]
